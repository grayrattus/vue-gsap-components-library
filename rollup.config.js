import vue from 'rollup-plugin-vue';
import peerDepsExternal from 'rollup-plugin-peer-deps-external';
import css from 'rollup-plugin-css-only';

export default [
  {
    input: 'src/index.js',
    output: [
      {
        format: 'esm',
        file: 'dist/library.mjs',
	assetFileNames: 'assets/[name].css'
      },
      {
        format: 'cjs',
        file: 'dist/library.js',
	assetFileNames: 'assets/[name].css'
      },
    ],
    plugins: [
      vue({ css: false }), css({}), peerDepsExternal(),
    ],
  },
];
