export default {
  themeConfig: {
    displayAllHeaders: true, 
    sidebar: [
      ['/components/horizontal-intro', 'Horizontal intro']
      ['/components/full-page', 'Full page']
    ]
  }
}

