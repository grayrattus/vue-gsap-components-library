import { defineClientConfig } from '@vuepress/client';
import ComponentLibrary from './../../src/index.js';
import Demo from './components/Demo.vue';
import SourceCode from './components/SourceCode.vue';

import InputTextDoc from './components/examples/input-text-doc.vue';
import HorizontalIntroDoc from './components/examples/horizontal-into-doc.vue';
import FullPageDoc from './components/examples/full-page-doc.vue';
import SpecialLayout from './components/SpecialLayout.vue';
import Dummy from './components/Dummy.vue';

export default defineClientConfig({
  enhance({app, router, siteData}) {
    app.use(ComponentLibrary);
    app.component('Demo', Demo);
    app.component('SourceCode', SourceCode);
    app.component('SourceCode', SourceCode);
    app.component('input-text-doc', InputTextDoc);
    app.component('horizontal-intro-doc', HorizontalIntroDoc);
    app.component('full-page-doc', FullPageDoc);
    app.component('dummy', Dummy);
  },
  layouts: {
    SpecialLayout
  },
  setup() {},
  rootComponents: [],
})
