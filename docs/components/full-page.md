---
layout: SpecialLayout
---

# full-page
This is a page which shows all components used in my portfolio

## Example

<Demo componentName="full-page-doc" />
