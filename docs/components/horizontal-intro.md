---
layout: SpecialLayout
---

# horizontal-intro
Horizontal intro component is used as the main component on the page that starts the story.

## Example

<Demo componentName="horizontal-intro-doc" />
<dummy />
