---
home: true
navbar: true
actions:
  - text: Get Started
    link: /guide
    type: primary
features:
  - title: Use GSAP
    details: The best animation library ever 
  - title: Use Vue.js
    details: The libray I'm currently learning
  - title: Use your brain
footer: Vue GSAP components library for creative developers 
---
